use graphql_client::{GraphQLQuery, reqwest::post_graphql};
use clap::Parser;
//use std::error::Error;
use chrono::NaiveDate;
use chrono::format::ParseError;

type JSON = String;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "queries/schema.graphql",
    query_path = "queries/acknowledgement_subitems.graphql",
    response_derives = "Debug",
)]
pub struct AcknowledgementSubitems;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    #[clap(short, long)]
    token: String,

    #[clap(short, long)]
    board: i64
}

#[tokio::main]
async fn main() {

    let args = Args::parse();

    //let orders = fetch_monday(&args.token, args.board, 1).await;
    let orders = fetch_monday(&args.token, args.board, 7).await;

    //println!("{:#?}", orders);
}

#[derive(Debug)]
enum Status {
    Ordered,
    Received,
    Unknown,
}

#[derive(Debug)]
enum Progress {
    Unknown,
}

#[derive(Debug)]
struct Material {
    id: i64,
    name: String,
    status: Status,
    vendor: Option<String>,
    order_date: Option<NaiveDate>,
    received_date: Option<NaiveDate>,
}

#[derive(Debug)]
struct Order {
    id: i64,
    name: String,
    due_date: Option<NaiveDate>,
    progress: Progress,
    acknowledgement: Option<String>,
    materials: Option<Vec<Material>>
}

async fn fetch_monday(token: &str, board: i64, page: i64) -> Vec<Order> {
    let variables = acknowledgement_subitems::Variables{
        page,
        board,
    };

    let mut headers = reqwest::header::HeaderMap::new();

    let mut auth_value = reqwest::header::HeaderValue::from_str(token).unwrap();
    auth_value.set_sensitive(true);
    headers.insert(reqwest::header::AUTHORIZATION, auth_value);
    
    let client = reqwest::Client::builder()
        .default_headers(headers)
        .build().unwrap();

    let response_body = post_graphql::<AcknowledgementSubitems, _>(&client,
                                                                   "https://api.monday.com/v2/",
                                                                   variables).await.unwrap();


    println!("{:#?}",&response_body);
    
    let mut orders: Vec<Order> = Vec::new();

    let board_data = response_body.data.unwrap().boards.unwrap();
    for b in board_data {
        for o in b.unwrap().items.unwrap() {
            let ord = o.unwrap();

            let mut due_date: Option<NaiveDate> = None;
            let mut progress = Progress::Unknown;
            let mut acknowledgement: Option<String> = None;
            
            for i in ord.column_values.unwrap() {
                let item = i.unwrap();
                
                match item.id.as_str() {
                    "due_date" => { due_date = if let Ok(date)
                                   = NaiveDate::parse_from_str(&item.text.unwrap(), "%Y-%m-%d") {
                                       Some(date)
                                   } else {
                                       None
                                   }
                    },
                    "progress" => {
                            match item.text.as_deref() {
                                Some("") => progress = Progress::Unknown,
                                None => progress = Progress::Unknown,
                                u => { panic!("{:?}", u)}
                            }
                    },
                    "text9" => {acknowledgement = item.text},
                    _ => {},
                }
            }

            let mut mats: Vec<Material> = Vec::new();
            if let Some(mat) = ord.subitems {
                for m in mat {
                    let mat = m.unwrap();

                    //println!("{:#?}",&mat);

                    let mut order_date: Option<NaiveDate> = None;
                    let mut received_date: Option<NaiveDate> = None;
                    let mut status = Status::Unknown;
                    let mut vendor: Option<String> = None;

                    for i in mat.column_values.unwrap() {
                        let item = i.unwrap();

                        match item.id.as_str() {
                            "status" => {
                                match item.text.as_deref() {
                                    Some("Is here") => status = Status::Received,
                                    Some("Ordered") => status = Status::Ordered,
                                    Some("") => status = Status::Unknown,
                                    None => status = Status::Unknown,
                                    u => { panic!("{:?}", u)}
                                }
                            },
                            "text9" => { vendor = item.text },
                            "date0" => { order_date = if let Ok(date)
                                        = NaiveDate::parse_from_str(&item.text.unwrap(), "%Y-%m-%d") {
                                            Some(date)
                                        } else {
                                            None
                                        }
                            },
                            "date" => { received_date = if let Ok(date)
                                       = NaiveDate::parse_from_str(&item.text.unwrap(), "%Y-%m-%d") {
                                           Some(date)
                                       } else {
                                           None
                                       }
                            },
                            _ => {},
                        }
                    }
                    
                    mats.push(Material {
                        id: mat.id.parse::<i64>().unwrap(),
                        name: mat.name,
                        status,
                        vendor,
                        order_date,
                        received_date,
                    });
                }
            }

            orders.push(
                Order {
                    id: ord.id.parse::<i64>().unwrap(),
                    name: ord.name,
                    due_date,
                    progress,
                    acknowledgement,
                    materials: if mats.len() > 0 { Some(mats) } else { None },
                }
            )
        }
    }

    orders
}
